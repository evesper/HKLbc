# Hertz-Knudsen-Langmuir boundary condition

The implemented OpenFoam-v1806 boundary condition provides a total pressure condition limited by the Hertz-Knudsen-Langmuir relation.